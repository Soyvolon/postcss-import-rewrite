# postcss-import-rewrite

A simple import statement rewriter that was to be developed so Blazor css isolation @import statements wont cause problems
when using postcss and tailwindcss. It probably has other uses.

## Configuration Options

Example:

```js
{
    envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
    ],
    rewrites: [
        {
            template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
            match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g,
            disablePathFix: false
        },
        {
            template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
            match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g,
            disablePathFix: false
        }
    ],
    deep: false
}
```

| Name             | Description                                                                                                        |
| ---------------- | ------------------------------------------------------------------------------------------------------------------ |
| `envvars`        | An array for environment variables to pull values from.                                                            |
| `rewrites`       | An array of `template`/`match` objects.                                                                            |
| `template`       | The replacement template. Use `{0}` for replacement values, where the number is the index in the `envvars` array   |
| `match`          | A regex for finding the start of an `@import` statement. The example looks for the string `@import '_content ...`. |
| `deep`           | If true, open existing `@import` files and process their css import statements as well.                            |
| `disablePathFix` | If true, disables path rewrites to match operating system.                                                         |

Some notes:

- Any capture groups in the `match` param will be appended to the `envvars` list in the order they are matched.
