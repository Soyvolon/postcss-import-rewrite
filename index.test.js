const postcss = require('postcss')
const { equal } = require('node:assert')
const { test } = require('node:test')
const fs = require('fs')
const path = require('path')

const plugin = require('./')

const pathRegex = /[\\/]/g

function getSafeOutput(output) {
  return output.replace(pathRegex, path.sep);
}

async function run(input, output, opts = {}) {
  let result = await postcss([plugin(opts)]).process(input, { from: path.resolve('./app.css') })
  equal(getSafeOutput(result.css), getSafeOutput(output))
  equal(result.warnings().length, 0)
}

test('Test @import rewrite', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Debug';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some\\Folder';

  await run(
    "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';\n" +
    "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.styles.css';",
    "@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Debug\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';\n" +
    "@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Debug\\net8.0\\scopedcss\\bundle\\BlackboxWorkflowInstaller.UI.Framework.styles.css';",
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
          }
      ],
      log: true,
      logFile: './log/debug.log',
    }
  );
});

test('Test @import rewrite', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Release';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some\\Folder';

  await run(
`@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';

/* _content/BlackboxWorkflowInstaller.UI/Main/UIRouter.razor.rz.scp.css */
body[b-z4f67jesf1] {
}
`,

`@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Release\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';

/* _content/BlackboxWorkflowInstaller.UI/Main/UIRouter.razor.rz.scp.css */
body[b-z4f67jesf1] {
}
`,
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
          }
      ],
      log: true,
      logFile: './log/debug.log',
    }
  );
});

test('Test @import rewrite for Files', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Release';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some\\Folder';

  const test = 'test_data/rel.css';

  if (fs.existsSync(test)) {
    fs.unlinkSync(test);
  }

  fs.mkdirSync('test_data', { recursive: true });
  fs.writeFileSync(test, "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';");

  await run(
    `@import '${test}';`,
    `@import '${test}';`,
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
          }
      ],
      log: true,
      logFile: './log/debug.log',
      deep: true
    }
  );

  const data = fs.readFileSync(test);

  equal(data, getSafeOutput("@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Release\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';"))
});

test('Test @import rewrite for Deep Files', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Release';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some\\Folder';

  const test = 'test_data/rel-deep.css';
  const test2 = 'test_data/rel-deep2.css';

  if (fs.existsSync(test)) {
    fs.unlinkSync(test);
  }

  if (fs.existsSync(test2)) {
    fs.unlinkSync(test2);
  }

  fs.mkdirSync('test_data', { recursive: true });
  fs.writeFileSync(test2, "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';");
  fs.writeFileSync(test, `@import '${path.resolve(test2)}';`);

  await run(
    `@import '${test}';`,
    `@import '${test}';`,
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
          }
      ],
      log: true,
      logFile: './log/debug.log',
      deep: true
    }
  );

  const data = fs.readFileSync(test2);

  equal(data, getSafeOutput("@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Release\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';"))
});

test('Test @import rewrite for Paths', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Release';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some/Folder';

  const test = 'test_data/rel-path-deep.css';
  const test2 = 'test_data/rel-path-deep2.css';

  if (fs.existsSync(test)) {
    fs.unlinkSync(test);
  }

  if (fs.existsSync(test2)) {
    fs.unlinkSync(test2);
  }

  fs.mkdirSync('test_data', { recursive: true });
  fs.writeFileSync(test2, "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';");
  fs.writeFileSync(test, `@import '${path.resolve(test2)}';`);

  await run(
    `@import '${test}';`,
    `@import '${test}';`,
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g,
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g,
          }
      ],
      log: true,
      logFile: './log/debug.log',
      deep: true
    }
  );

  const data = fs.readFileSync(test2);

  equal(data, getSafeOutput("@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Release\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';"))
});

test('Test @import rewrite with blind matching.', async () => {
  process.env['DOTNET_CONFIG_NAME'] = 'Release';
  process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
  process.env['DOTNET_PROJECT_DIR'] = 'Some/Folder';

  const test = 'test_data/rel-path-blind.css';
  const test2 = 'test_data/rel-path-blind2.bundle.scp.css';

  if (fs.existsSync(test)) {
    fs.unlinkSync(test);
  }

  if (fs.existsSync(test2)) {
    fs.unlinkSync(test2);
  }

  fs.mkdirSync('test_data', { recursive: true });
  fs.writeFileSync(test2, "@import '_content/BlackboxWorkflowInstaller.UI.Framework/BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';");
  let name = path.join(path.dirname(path.resolve(test2)), 'bad-file.bundle.scp.css');
  fs.writeFileSync(test, `@import '${name}';`);

  await run(
    `@import '${test}';`,
    `@import '${test}';`,
    {
      envvars: [
        'DOTNET_PROJECT_DIR',
        'DOTNET_CONFIG_NAME',
        'DOTNET_TARGET_FRAMEWORK',
      ],
      rewrites: [
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g,
          },
          {
              template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
              match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g,
          }
      ],
      log: true,
      logFile: './log/debug.log',
      deep: true,
      blindMatch: /(\.bundle\.scp\.css)|(\.styles\.css)$/gm
    }
  );

  const data = fs.readFileSync(test2).toString();

  equal(data, getSafeOutput("@import 'Some\\Folder\\BlackboxWorkflowInstaller.UI.Framework\\obj\\Release\\net8.0\\scopedcss\\projectbundle\\BlackboxWorkflowInstaller.UI.Framework.bundle.scp.css';"))
});
