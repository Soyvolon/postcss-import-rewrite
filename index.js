const postcss = require('postcss')
const fs = require('fs');
const path = require('path');

/**
 * @type {import('postcss').PluginCreator}
 */
module.exports = (opts = {}) => {
  // Work with options here

  // https://stackoverflow.com/a/18234317/11682098
  String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
    function () {
      "use strict";
      var str = this.toString();
      if (arguments.length) {
        var t = typeof arguments[0];
        var key;
        var args = ("string" === t || "number" === t) ?
          Array.prototype.slice.call(arguments)
          : arguments[0];

        for (key in args) {
          str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
        }
      }

      return str;
    };

  async function log(msg) {
    if (opts.log) {
      console.log(msg);

      if (opts.logFile) {
        await logToFile(msg);
      }
    }
  }

  async function logToFile(msg) {
    const dir = path.dirname(opts.logFile);
    await fs.promises.mkdir(dir, { recursive: true });
    await fs.promises.appendFile(opts.logFile, '\n' + msg);
  }

  const processed = Symbol('processed');
  const pathRegex = /[\\/]/g

  return {
    postcssPlugin: 'postcss-import-rewrite',

    async Once(root, { AtRule }) {
      try {
        if (root[processed]) {
          return;
        }

        root[processed] = true;

        for (let x = 0; x < root.nodes.length; x++) {
          const node = root.nodes[x];

          for (let y = 0; y < opts.rewrites.length; y++) {
            const rewrite = opts.rewrites[y];

            let param = node.params;

            if (param && param.match(rewrite.match)) {
              let envvars = [];

              for (let i = 0; i < opts.envvars.length; i++) {
                const varname = opts.envvars[i];
                envvars.push(process.env[varname]);
              }

              log("ENV VARS: " + envvars.join(" | "));

              let matches = param.matchAll(rewrite.match);
              for (const match of matches) {
                let data = match.slice(1);
                for (const i of data) {
                  envvars.push(i);
                }
              }

              let res = '';
              if (rewrite.disablePathFix) {
                res = rewrite.template.formatUnicorn(envvars);
              } else {
                let dest = rewrite.template.formatUnicorn(envvars);
                dest = dest.replace(pathRegex, path.sep);
                res = dest;
              }

              node.params = res;

              if (opts.log) {
                const msg = `Processed ${AtRule} for ${node} with result: ${res}`;
                await log(msg);
              }

              break;
            }
          }

          if (node.name === 'import' && opts.deep) {
            const fileName = node.params.slice(1, -1);

            log('checking file ' + fileName)

            let file = "";
            if (path.isAbsolute(fileName)) {
              file = fileName;
            } else {
              const relDir = path.dirname(root.source.input.file);
              file = path.join(relDir, fileName);
            }

            let exists = fs.existsSync(file);
            if (!exists && opts.blindMatch) {
              let dir = path.dirname(file);
              if (fs.existsSync(dir)) {
                const dirData = fs.readdirSync(dir);
                let blindFile = dirData.filter(e => e.match(opts.blindMatch)).find(() => true);

                if(blindFile) {
                  blindFile = path.join(dir, blindFile);
                  exists = fs.existsSync(blindFile);
                }

                if (exists) {
                  file = blindFile;
                  node.params = `'${file}'`;

                  const msg = `Blind matched to ${file}.`;
                  await log(msg);
                }
              }
            }

            if (exists) {
              const msg = `Moving to file: ${file} for further processing.`;
              await log(msg);

              let data = await fs.promises.readFile(file);
              try {
                let res = await postcss([module.exports(opts)]).process(data, { from: file });
                await fs.promises.writeFile(file, res.css);
              } catch (error) {
                log("Something went wrong with processing: " + error)
              }
            } else {
              const msg = `Skipped: ${file} - file not found.`;
              await log(msg);
            }
          }
        }
      } catch (error) {
        console.log(error.stack);
      }
    }
  }
}

module.exports.postcss = true
