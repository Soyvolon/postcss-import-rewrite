# Changelog

## 1.4.1

Fixed missing quotes around blindly matched import statement.

## 1.4.0

Added support for files that don't have exact matches but, using a regex, match a condition for wildcard matches in the same destination folder.

## 1.3.2

Added a fix for ensuing paths are to be navigated for the current operating system.

## 1.3.1

Fixed some errors with processing absolute file paths and relative file paths.

## 1.3.0

Added support for exploring `@import` statements to find more statements to replace.

## 1.2.0

Added support for multiple match/template patterns.

## 1.1.0

Expanded the use of regex and string modification to fit better with use cases in postcss.

## 1.0.0

Added initial code.
